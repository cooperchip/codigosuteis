```
/*
	C�digo para gerar combina��es de n elementos, tomados p a p;
	By: Juan Lopes (Dot.Net Brasil)
	Em: jun/2013
*/
```

```
class MainClass
{
	// n = 25 (Espa�o Amostral - possibilidade m�xima de numeros disponivel por cart�o)
	// int[] C = array com 15 elementos (numero m�ximo de escolha por aposta)
	public static bool Next (int n, int[] C)
	{
		// Recebe tamanho do Array (15)
		int k = C.Length;
		// Itera de 14 (�ltimo �ndice) at� 0 (primeiro �ndice): total = 15.
		// Neste Caso: Para i de 15-1 at� 0, subtrai-se 1;
		for (int i = k-1; i>=0; i--) {
			// 
			if (++C [i] <= i + n - k) {
				for (int j=i+1; j<k; j++)
				C [j] = C [i] + j - i;

				for (int j=1; j<k; j++)
				C [j] = Math.Max (C [j - 1] + 1, C [j]);

				return true;
			}
		}
		return false;
	}

	public static void Main ()
	{
		int[] C = new int[15];
		int i = 0;
		while (Next(25, C)) {
			//Console.WriteLine (string.Join (", ", C.Select (x => x.ToString ()).ToArray ()));
			i++;
		}
		Console.WriteLine (i);
	}
}
```